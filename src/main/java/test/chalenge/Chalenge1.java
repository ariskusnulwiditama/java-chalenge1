package test.chalenge;
import java.util.Scanner;

public class Chalenge1 {

    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
//    select menu
        mainMenu();


        int pilihan = scanner.nextInt();
        while (pilihan != 0){
            switch (pilihan){
                case 1:
                   luasBidang();
                    int pilihan1 = scanner.nextInt();
                    switch (pilihan1){
                        case 0:
                            mainMenu();
                            pilihan = scanner.nextInt();
                            break;
                        case 1:
                            luasPersegi();
                            break;
                        case 2:
                            luasLingkaran();
                            break;
                        case 4:
                            luasPersegiPanjang();
                            break;
                        case 3:
                            luasSegitiga();
                            break;

                        default:
                            System.out.println("Pilihan tidak ada");
                            break;
                    }
                    break;
                case 2:
                    volumeRuang();
                    int pilihan2 = scanner.nextInt();
                    switch (pilihan2){
                        case 0:
                            mainMenu();
                            clearScreen();
                            pilihan = scanner.nextInt();
                            break;
                        case 1:
                           volumeKubus();
                            break;
                        case 3:
                           volumeTabung();
                           break;
                        case 2:
                           volumeBalok();
                            break;

                        default:
                            System.out.println("Pilihan tidak ada");

                    }
                    break;
                default:
                    System.out.println("Pilihan tidak ada");
                    pilihan = 0;
                    break;
            }
        }
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void mainMenu() {
        System.out.println("---------------------------------");
        System.out.println("kalkulator Penghitung Luas dan Volum");
        System.out.println("---------------------------------");
        System.out.println("1. Bangun Datar");
        System.out.println("2. Bangun Ruang");
        System.out.println("0. Tutup aplikasi");
        System.out.println("---------------------------------");
        System.out.print("Pilihan : ");
    }

    public static void luasBidang() {
        System.out.println("----------------------------------------");
        System.out.println("Pilih Bidang yang akan dihitung");
        System.out.println("----------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("----------------------------------------");
        System.out.print("Pilihan : ");
    }

    public static void volumeRuang() {
        System.out.println("------------------------------------------");
        System.out.println("Pilih Bangun Ruang yang akan dihitung");
        System.out.println("------------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("------------------------------------------");
        System.out.print("Pilihan : ");
    }

    public static void luasPersegi() {
        System.out.print("Masukkan sisi : ");
        int sisi = scanner.nextInt();
        System.out.println("Luas Persegi : " + Math.ceil(sisi * sisi));
    }

    public static void luasLingkaran() {
        System.out.print("Masukkan Jari-jari : ");
        int jari = scanner.nextInt();
        System.out.println("Luas Lingkaran = " + Math.ceil(Math.PI * jari * jari));
    }

    public static void luasPersegiPanjang() {
        System.out.print("Masukkan panjang : ");
        int panjang = scanner.nextInt();
        System.out.print("Masukkan lebar : ");
        int lebar = scanner.nextInt();
        System.out.println("Luas Persegi Panjang = " + Math.ceil(panjang * lebar));
    }

    public static void luasSegitiga() {
        System.out.print("Masukkan Alas : ");
        int alas = scanner.nextInt();
        System.out.print("Masukkan Tinggi : ");
        int tinggi = scanner.nextInt();
        System.out.println("Luas Segitiga = " + Math.ceil((alas * tinggi) / 2));
    }

    public static void volumeKubus() {
        System.out.print("Masukkan sisi : ");
        int sisi = scanner.nextInt();
        System.out.println("Volume Kubus = " + Math.ceil(sisi * sisi * sisi));
    }

    public static void volumeTabung() {
        System.out.print("Masukkan tinggi : ");
        int tinggi = scanner.nextInt();
        System.out.print("Masukkan jari-jari : ");
        int jari = scanner.nextInt();
        System.out.println("Volume Tabung = " + Math.ceil(Math.PI * jari * jari * tinggi));
    }

    public static void volumeBalok() {
        System.out.print("Masukkan panjang : ");
        int panjang = scanner.nextInt();
        System.out.print("Masukkan lebar : ");
        int lebar = scanner.nextInt();
        System.out.print("Masukkan tinggi : ");
        int tinggi1 = scanner.nextInt();
        System.out.println("Volume Balok = " + Math.ceil(panjang * lebar * tinggi1));
    }

}
